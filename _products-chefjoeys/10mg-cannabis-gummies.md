---
title: 10 mg Cannabis Gummies (10 pc bag)
prodcategory: GUMMIES
description: 10 mg Cannabis Gummies THC edibles
price: '4.00'
photo: photo-chefjoeys-gummies-medium-blueberry@2x.jpg
extraphotos:
  - photo-chefjoeys-gummies-medium-key_lime@2x.jpg
  - photo-chefjoeys-gummies-medium-mango@2x.jpg
  - photo-chefjoeys-gummies-medium-pineapple@2x.jpg
  - photo-chefjoeys-gummies-medium-strawberry@2x.jpg
---

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. 

10 PIECES PER BAG, 10 mg THC PER PIECE  
Net Weight 26 g (0.917 oz), 2.6 g Per Piece.