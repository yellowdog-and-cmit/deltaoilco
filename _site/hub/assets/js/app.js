//

var elementOld = null;
var openClass = "Accordion__tab--open";

function toggleAccordion(element) {
  content = element.querySelector(".Accordion__tab__content");

  if (elementOld != null) {
    elementOld.classList.remove(openClass);
    contentOld = elementOld.querySelector(".Accordion__tab__content");
    contentOld.style.maxHeight = "0px";
  }

  if (elementOld !== element) {
    element.classList.add(openClass);
    content.style.maxHeight = content.scrollHeight + "px";
    elementOld = element;
  } else {
    elementOld = null;
  }
}


$(window).on("load", function () {
  var url = window.location.pathname;
   url = url.slice(0, url.lastIndexOf('/'));     
   //salert(url);

  $("li.nav-cstm a").each(function () {
    console.log($(this).attr("href"));
    if ($(this).attr("href") == url) {
      $("li.nav-cstm.active").removeClass("active");
      $(this).parent().addClass("active");

    }
  });
});

$( document ).ready(function() {
  var loc = window.location.href; // returns the full URL
  if(/main/.test(loc)) {
    $('body').addClass('delta-main');
  }
});

$(window).scroll(function() {
if ($(this).scrollTop() > 200){  
    $('.navbar-menu').addClass("sticky");
  }
  else{
    $('.navbar-menu').removeClass("sticky");
  }
  if ($(this).scrollTop() > 200){  
    $('.navbar-start').addClass("sticky");
  }
  else{
    $('.navbar-start').removeClass("sticky");
  }
});