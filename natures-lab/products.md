---
layout: custom-layout
brand: natureslab
hide_hero: true
hide_footer: true
permalink: /natures-lab/products/
---
<section class="about-navbar">
  <nav class="navbar is-transparent natures-lab-nav natures-product-lab-nav-wrap custom-naturel" role="navigation" aria-label="main navigation">
    <div class="container is-max-desktop">
      <div class="navbar-start">
        <a class="navbar-item" href="/natures-lab/products/">
          Our Story
        </a>
        <a class="navbar-item" href="#">
          Lab Creations
        </a>
        <div class="navbar-brand">
          <a class="navbar-item" href="#">
            <img src="/img/logo-natures_lab-primary.svg">
          </a>
          <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div><a class="navbar-item" href="/natures-lab/products/">
          Products
        </a>
        <a class="navbar-item" href="#">
          Contact
        </a>
      </div>
    </div>
  </nav>
  <nav class="navbar is-transparent natures-lab-nav-in-mobile" role="navigation" aria-label="main navigation">
    <div class="container is-max-desktop">
      <div class="navbar-brand">
        <a class="navbar-item" href="#">
          <img src="/img/logo-natures_lab-primary.svg" style="
">
        </a>
        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
      <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">
          <a class="navbar-item" href="/natures-lab/products/">
            Our Story
          </a>
          <a class="navbar-item" href="#">
            Lab Creations
          </a>
          <a class="navbar-item" href="/natures-lab/products/">
            Products
          </a>
          <a class="navbar-item" href="#">
            Contact
          </a>
        </div>
      </div>
    </div>
  </nav>
</section>
<section class="section single-product-overview delta-single-product-overview-wrap delta-single-product-overview-wrap-inner-wrap naturel-single-custom">
  <div class="container">
    <h2 class="title">GUMMIES</h2>
    <div class="columns is-multiline">
      <div class="column is-6 Products-box">
        <a href="/natures-lab/products/concentrate/">
          <figure class="image">
            <img src="/img/product-nle-concentrate@2x.jpg" class="img-product-main">
            <h3 class="title">Concentrate</h3>
          </figure>
        </a>
      </div>
      <div class="column is-6 Products-box">
        <a href="/natures-lab/products/cartridges/">
          <figure class="image">
            <img src="/img/product-delta-concentrate@2x.jpg" class="img-product-main">
            <h3 class="title">Cartridges</h3>
          </figure>
        </a>
      </div>
      <div class="column is-6 Products-box">
        <a href="/natures-lab/products/concentrate/">
          <figure class="image">
            <img src="/img/product-nle-concentrate@2x.jpg" class="img-product-main">
            <h3 class="title">Concentrate</h3>
          </figure>
        </a>
      </div>
      <div class="column is-6 Products-box">
        <a href="/natures-lab/products/cartridges/">
          <figure class="image">
            <img src="/img/product-nle-concentrate@2x.jpg" class="img-product-main">
            <h3 class="title">Cartridges</h3>
          </figure>
        </a>
      </div>
    </div>
  </div>
</section>
<section class="section single-product-overview delta-single-product-overview-wrap delta-single-product-overview-wrap-inner-wrap naturel-single-custom">
  <div class="container">
    <h2 class="title">GUMMIES</h2>
    <div class="columns is-multiline">
      <div class="column is-6 Products-box">
        <a href="/natures-lab/products/concentrate/">
          <figure class="image">
            <img src="/img/product-nle-concentrate@2x.jpg" class="img-product-main">
            <h3 class="title">Concentrate</h3>
          </figure>
        </a>
      </div>
    </div>
  </div>
</section>
<section class="section natures-footer-top-sec">
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-12 has-text-centered">
        <h1 class="title">Updates from the Lab</h1>
        <p class="body1 mb-12">Be the first to know about new and limited products<br> and strains available in your area.</p>
        <p class="body2"><input class="input is-rounded" type="text" placeholder="">
          <a href="#"><button type="button" class="button is-primary">Subscribe</button></a>
        </p>
      </div>
    </div>
  </div>
</section>
<footer class="footer natures-footer-bottom-sec">
  <div class="container">
    <!---------------------------------------------stay know------------------------------------------------>
    <!---------------------------------------------footer chefgoey------------------------------------------------>
    <section class="section natures-footer-bottom-inner-sec">
      <div class="container">
        <div class="columns is-multiline is-centered">
          <div class="column is-3 has-text-centered"><a href="#"><img src="/img/logo-natures_lab-primary.svg" class="img-responsive"></a>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Products</h6>
            <ul>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
            </ul>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Resources</h6>
            <ul>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
            </ul>
          </div>
          <div class="column is-3 social-icon">
            <h6 class="title is-6">Community</h6>
            <ul>
              <li><a href="#"><img src="../../img/facebook.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="../../img/instagram.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="../../img/twitter.svg" class="img-responsive"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!---------------------------------------------Copyright footer chefgoey------------------------------------------------>
    <section class="footer-copyright-sec">
      <div class="container">
        <div class="columns is-multiline">
          <div class="column is-6 has-text-left">
            <p class="">© 2021, Nature’s Lab Oklahoma. A JDMG, LLC partnership.</p>
          </div>
          <div class="column is-6 has-text-right">
            <ul>
              <li><a href="#">License</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </div>
</footer>
<script>
$(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");

  });
});
</script>