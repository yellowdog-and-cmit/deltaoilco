---
layout: custom-layout
brand: natureslab
hide_hero: true
hide_footer: true
permalink: /natures-lab/products/concentrate/
---
<section class="about-navbar">
  <nav class="navbar is-transparent natures-lab-nav natures-product-lab-nav-wrap" role="navigation" aria-label="main navigation">
    <div class="container is-max-desktop">
      <div class="navbar-start">
        <a class="navbar-item" href="/natures-lab/products/">
          Our Story
        </a>
        <a class="navbar-item" href="#">
          Lab Creations
        </a>
        <div class="navbar-brand">
          <a class="navbar-item" href="#">
            <img src="/img/logo-natures_lab-primary.svg">
          </a>
          <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div><a class="navbar-item" href="/natures-lab/products/">
          Products
        </a>
        <a class="navbar-item" href="#">
          Contact
        </a>
      </div>
    </div>
  </nav>
  <nav class="navbar is-transparent natures-lab-nav-in-mobile natures-lab-nav-in-mobile-wrap" role="navigation" aria-label="main navigation">
    <div class="container is-max-desktop">
      <div class="navbar-brand">
        <a class="navbar-item" href="#">
          <img src="/img/logo-natures_lab-primary.svg" style="
">
        </a>
        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
      <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">
          <a class="navbar-item" href="/natures-lab/products/">
            Our Story
          </a>
          <a class="navbar-item" href="#">
            Lab Creations
          </a>
          <a class="navbar-item" href="/natures-lab/products/">
            Products
          </a>
          <a class="navbar-item" href="#">
            Contact
          </a>
        </div>
      </div>
    </div>
  </nav>
</section>
<section class="section product-page-product-content-section delta-product-single-page-warp natures-product-single-page-warp">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-5 Products-left-images">
        <a href="#">
          <figure class="image">
            <img src="/img/product-nle-concentrate@2x.jpg" class="img-product-main">
          </figure>
        </a>
        <ul class="products-left-light-image">
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-nle-sauce@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-nle-sauce@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-nle-sauce@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-nle-sauce@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-nle-sauce@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-nle-sauce@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-nle-sauce@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-nle-sauce@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
        </ul>
      </div>
      <div class="column is-7 Products-right-content">
        <h3 class="title">Full Spectrum Cannabis Oil</h3>
        <p class="body1 mb-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
        <p class="body1 mb-5"> Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. 10 PIECES PER BAG, 25 mg THC PER PIECE Net Weight 26 g (0.917 oz), 2.6 g Per Piece</p>
        <p class="body1 mb-12">10 PIECES PER BAG, 25 mg THC PER PIECE Net Weight 26 g (0.917 oz), 2.6 g Per Piece</p>
        <div class="Products-right-content-select-product">
          <h4 class="title is-4">Flavors</h4>
          <div class="select">
            <select>
              <option>Blueberry Punch</option>
              <option>With options</option>
            </select>
          </div>
          <a href="http://127.0.0.1:4000/order/">
            <button type="button" class="button is-primary">Request to Order</button>
          </a>
        </div>
        <div class="product-short-detail-logo-inner">
          <ul>
            <li>
              <img src="/img/Rectangle 73@2x.png" class="img-product-main">
            </li>
            <li>
              <img src="/img/Rectangle 70@2x.png" class="img-product-main">
            </li>
            <li>
              <img src="/img/Rectangle 77@2x.png" class="img-product-main">
            </li>
            <li>
              <img src="/img/Rectangle 75@2x.png" class="img-product-main">
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section single-product-overview delta-single-product-overview-wrap nature-single-product-overview-wrap">
  <div class="container">
    <h2 class="title">GUMMIES</h2>
    <div class="columns is-multiline">
      <div class="column is-4 Products-box">
        <a href="/natures-lab/products/cartridges/">
          <figure class="image">
            <img src="/img/product-nle-concentrate@2x.jpg" class="img-product-main">
            <h3 class="title">Cartridges</h3>
          </figure>
        </a>
      </div>
      <div class="column is-4 Products-box">
        <a href="/natures-lab/products/concentrate/">
          <figure class="image">
            <img src="/img/product-nle-concentrate@2x.jpg" class="img-product-main">
            <h3 class="title">Concentrate</h3>
          </figure>
        </a>
      </div>
      <div class="column is-4 Products-box">
        <a href="/natures-lab/products/cartridges/">
          <figure class="image">
            <img src="/img/product-nle-concentrate@2x.jpg" class="img-product-main">
            <h3 class="title">Cartridges</h3>
          </figure>
        </a>
      </div>
    </div>
  </div>
</section>
<!---------------------------------------------Copyright footer delta------------------------------------------------>
<section class="section natures-footer-top-sec">
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-12 has-text-centered">
        <h1 class="title">Updates from the Lab</h1>
        <p class="body1 mb-12">Be the first to know about new and limited products<br> and strains available in your area.</p>
        <p class="body2"><input class="input is-rounded" type="text" placeholder="">
          <a href="#"><button type="button" class="button is-primary">Subscribe</button></a>
        </p>
      </div>
    </div>
  </div>
</section>
<footer class="footer natures-footer-bottom-sec">
  <div class="container">
    <!---------------------------------------------stay know------------------------------------------------>
    <!---------------------------------------------footer chefgoey------------------------------------------------>
    <section class="section natures-footer-bottom-inner-sec">
      <div class="container">
        <div class="columns is-multiline is-centered">
          <div class="column is-3 has-text-centered"><a href="#"><img src="/img/logo-natures_lab-primary.svg" class="img-responsive"></a>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Products</h6>
            <ul>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
            </ul>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Resources</h6>
            <ul>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
            </ul>
          </div>
          <div class="column is-3 social-icon">
            <h6 class="title is-6">Community</h6>
            <ul>
              <li><a href="#"><img src="/img/facebook.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="/img/instagram.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="/img/twitter.svg" class="img-responsive"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!---------------------------------------------Copyright footer chefgoey------------------------------------------------>
    <section class="footer-copyright-sec">
      <div class="container">
        <div class="columns is-multiline">
          <div class="column is-6 has-text-left">
            <p class="">© 2021, Nature’s Lab Oklahoma. A JDMG, LLC partnership.</p>
          </div>
          <div class="column is-6 has-text-right">
            <ul>
              <li><a href="#">License</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </div>
</footer>
<script>
$(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");

  });
});
</script>