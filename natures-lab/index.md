---
layout: custom-layout
brand: natureslab
hide_hero: true
hide_footer: true
permalink: /natures-lab/
---
<nav class="navbar is-transparent natures-lab-nav" role="navigation" aria-label="main navigation">
  <div class="container is-max-desktop">
    <div class="navbar-start">
      <a class="navbar-item" href="products">
        Our Story
      </a>
      <a class="navbar-item" href="#">
        Lab Creations
      </a>
      <div class="navbar-brand">
        <a class="navbar-item" href="#">
          <img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/logo-natures_lab-primary.svg">
        </a>
        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div><a class="navbar-item" href="products">
        Products
      </a>
      <a class="navbar-item" href="#">
        Contact
      </a>
    </div>
  </div>
</nav>
<nav class="navbar is-transparent natures-lab-nav-in-mobile" role="navigation" aria-label="main navigation">
  <div class="container is-max-desktop">
    <div class="navbar-brand">
      <a class="navbar-item" href="#">
        <img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/logo-natures_lab-primary.svg" style="
">
      </a>
      <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>
    <div id="navbarBasicExample" class="navbar-menu">
      <div class="navbar-start">
        <a class="navbar-item" href="products">
          Our Story
        </a>
        <a class="navbar-item" href="#">
          Lab Creations
        </a>
        <a class="navbar-item" href="products">
          Products
        </a>
        <a class="navbar-item" href="#">
          Contact
        </a>
      </div>
    </div>
  </div>
</nav>
<section class="natures-lab-banner pt-5 pb-5">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-6 natures-lab-banner-left-content">
        <h1 class="title pb-4">The world’s finest<br> Sauce comes from<br> Nature’s Lab</h1>
      </div>
      <div class="column is-6 natures-lab-banner-right-content">
      </div>
    </div>
  </div>
  <section class="section natures-lab-about-home">
    <div class="container is-max-desktop">
      <div class="columns is-desktop is-centered">
        <div class="column is-5">
          <figure class="image"> <img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/photo-nle-bio@2x.jpg" class="img-responsive"> </figure>
        </div>
        <div class="column is-7 natures-lab-about-home-right-sec">
          <p class="body1 mb-5">Founded by Marcus Moates & Ross Norman in 2008 in Los Angeles, California, Nature’s Lab set the bar for producing the world’s finest “Sauce”. With 56 trophy wins at Hightimes & international events, our ever-evolving lab work creating solvent free products made from premium flower strains with a goal of 0ppm and excellent flavor continues to raise the bar in the cannabis industry.</p>
          <p class="body1">In 2020, an OKBUDHUB and Nature’s Lab partnership brought the world’s finest sauce to the state of Oklahoma.</p>
        </div>
      </div>
    </div>
  </section>
</section>
<section class="section natures-product-home-inner">
  <h1 class="title has-text-centered natures-product-home-heading-main">Our Lab Creations</h1>
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-3">
        <figure class="image"> <img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/photo-nle-sauce@2x.jpg" class="img-responsive"> </figure>
      </div>
      <div class="column is-9">
        <h3 class="title-h3">NLE Sauce</h3>
        <p class="body1">Sauce or High Terpene Full Spectrum Extract is made by separating the terpene compounds from the cannabinoid molecules. This separation causes the majority of cannabinoids to crystalize forming small diamond like formations and the terpenes to stay suspended in a more liquid stated. This type of concentrate gives the user the most control over the dabbing experience by allowing them to change the ratio of cannabinoids to terpenes consumed in a single dab.</p>
      </div>
    </div>
    <div class="columns is-desktop is-centered">
      <div class="column is-3">
        <figure class="image"> <img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/photo-nle-shatter@2x.jpg" class="img-responsive"> </figure>
      </div>
      <div class="column is-9">
        <h3 class="title-h3">NLE Shatter</h3>
        <p class="body1">Shatter is a completely translucent cannabis extract which is a fully homogenized expression of the plant’s terpene and cannabinoid profiles. Almost fully stable in most cases this concentrate is very easy to handle and consume.</p>
      </div>
    </div>
    <div class="columns is-desktop is-centered">
      <div class="column is-3">
        <figure class="image"> <img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/photo-nle-crumble@2x.jpg" class="img-responsive"> </figure>
      </div>
      <div class="column is-9">
        <h3 class="title-h3">NLE Crumble</h3>
        <p class="body1">Our flagship product that launched our brand, crumble is made with heat &amp; agitation, which has a thicker cheese-like body with a rich fragrance.</p>
      </div>
    </div>
  </div>
</section>
<section class="section natures-product-details-as-main-outer">
  <h1 class="title has-text-centered natures-product-details-as">Our Products</h1>
  <div class="container">
    <div class="columns is-desktop is-centered">
      <div class="column is-4">
        <figure class="image"> <a href="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/products/concentrate/"><img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/product-nle-concentrate@2x.jpg" class="img-responsive"></a> </figure>
        <h3 class="title"><a href="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/products/concentrate/" class="natures-product-details-as-heading-content">Concentrate</a></h3>
      </div>
      <div class="column is-4">
        <figure class="image"> <a href="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/products/cartridges/"><img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/product-nle-concentrate@2x.jpg" class="img-responsive"></a> </figure>
        <h3 class="title"><a href="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/products/cartridges/" class="natures-product-details-as-heading-content">Cartridges</a></h3>
      </div>
      <div class="column is-4">
        <figure class="image"> <a href="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/products/concentrate/"><img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/product-nle-concentrate@2x.jpg" class="img-responsive"></a> </figure>
        <h3 class="title"><a href="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/products/concentrate/" class="natures-product-details-as-heading-content">Concentrate</a></h3>
      </div>
    </div>
  </div>
</section>
<section class="section natures-footer-top-sec">
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-12 has-text-centered">
        <h1 class="title">Updates from the Lab</h1>
        <p class="body1 mb-12">Be the first to know about new and limited products<br> and strains available in your area.</p>
        <p class="body2"><input class="input is-rounded" type="text" placeholder="">
          <a href="#"><button type="button" class="button is-primary">Subscribe</button></a>
        </p>
      </div>
    </div>
  </div>
</section>
<footer class="footer natures-footer-bottom-sec">
  <div class="container">
    <!---------------------------------------------stay know------------------------------------------------>
    <!---------------------------------------------footer chefgoey------------------------------------------------>
    <section class="section natures-footer-bottom-inner-sec">
      <div class="container">
        <div class="columns is-multiline is-centered">
          <div class="column is-3 has-text-centered"><a href="#"><img src="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/assets/images/logo-natures_lab-primary.svg" class="img-responsive"></a>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Products</h6>
            <ul>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
            </ul>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Resources</h6>
            <ul>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
            </ul>
          </div>
          <div class="column is-3 social-icon">
            <h6 class="title is-6">Community</h6>
            <ul>
              <li><a href="#"><img src="{{ site.url-hub }}{{ site.devbaseurl-hub }}/assets/images/facebook.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="{{ site.url-hub }}{{ site.devbaseurl-hub }}/assets/images/instagram.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="{{ site.url-hub }}{{ site.devbaseurl-hub }}/assets/images/twitter.svg" class="img-responsive"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!---------------------------------------------Copyright footer chefgoey------------------------------------------------>
    <section class="footer-copyright-sec">
      <div class="container">
        <div class="columns is-multiline">
          <div class="column is-6 has-text-left">
            <p class="">© 2021, Nature’s Lab Oklahoma. A JDMG, LLC partnership.</p>
          </div>
          <div class="column is-6 has-text-right">
            <ul>
              <li><a href="#">License</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </div>
</footer>
<script>
$(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");

  });
});
</script>