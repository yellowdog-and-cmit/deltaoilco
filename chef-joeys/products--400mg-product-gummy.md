---
layout: custom-layout
brand: chefjoeys
hide_hero: true
hide_footer: true
permalink: /chef-joeys/products/400mg-product-gummies/
---
<nav class="navbar is-transparent chef-joeys-nav" role="navigation" aria-label="main navigation">
  <div class="container is-max-desktop">
    <div class="navbar-brand">
      <a class="navbar-item" href="/chef-joeys">
        <img src="/img/logo-chef_joeys-menu-logo-header.png">
      </a>
      <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>
    <div id="navbarBasicExample" class="navbar-menu">
      <div class="navbar-start">
        <a class="navbar-item" href="/chef-joeys/products/">
          PRODUCTS
        </a>
        <a class="navbar-item" href="#">
          ABOUT THE CHEF
        </a>
        <a class="navbar-item" href="#">
          CONTACT
        </a>
      </div>
    </div>
  </div>
</nav>
<div class="col-md-12 has-text-centered chefjoey-product-top-heading">
  <h2 class="title">GUMMIES</h2>
</div>
<section class="section product-page-product-content-section">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-5 Products-left-images">
        <a href="#">
          <figure class="image">
            <img src="/img/product-chefjoeys-gummies-medium@2x.jpg" class="img-product-main">
          </figure>
        </a>
        <ul class="products-left-light-image">
          <li><a href="#">
              <figure class="image">
                <img src="/img/product-chefjoeys-gummies-medium@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-chefjoeys-gummies-medium-blueberry@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-chefjoeys-gummies-medium-key_lime@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-chefjoeys-gummies-medium-mango@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-chefjoeys-gummies-medium-pineapple@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-chefjoeys-gummies-medium-strawberry@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-chefjoeys-gummies-medium-strawberry@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/photo-chefjoeys-gummies-medium-strawberry@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
        </ul>
      </div>
      <div class="column is-7 Products-right-content">
        <h3 class="title">400 mg Cannabis Gummies (10 pc bag)</h3>
        <p class="body1 mb-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
        <p class="body1 mb-5"> Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. 10 PIECES PER BAG, 400 mg THC PER PIECE Net Weight 26 g (0.917 oz), 2.6 g Per Piece</p>
        <p class="body1 mb-12">10 PIECES PER BAG, 400 mg THC PER PIECE Net Weight 26 g (0.917 oz), 2.6 g Per Piece</p>
        <div class="Products-right-content-select-product">
          <h4 class="title is-4">Flavors</h4>
          <div class="select">
            <select>
              <option>Blueberry Punch</option>
              <option>With options</option>
            </select>
          </div>
          <a href="http://127.0.0.1:4000/order/">
            <button type="button" class="button is-primary">Request to Order</button>
          </a>
        </div>
        <div class="Products-right-content-product-short-decription">
          <div class="Products-short-decription-left"><img src="/img/product-chefjoeys-gummies-medium@2x.jpg" class="img-product-main"></div>
          <div class="Products-short-decription-right">
            <h6 class="title is-6">THIS PRODUCT HAS BEEN TESTED FOR CONTAMINANTS.</h6><strong> WARNING:</strong><span class="content-short-pragraph">Women should not use marijuana or medical marijuana products during pregnancy because of the risk of birth defects or while breastfeeding. </span>
            <h6 class="title is-6">KEEP OUT OF REACH OF CHILDREN.</h6><strong>FOR ACCIDENTAL INGESTION CALL 1-800-222-1222</strong>
          </div>
        </div>
        <div class="product-short-detail-logo-inner">
          <ul>
            <li>
              <img src="/img/Rectangle 73@2x.png" class="img-product-main">
            </li>
            <li>
              <img src="/img/Rectangle 70@2x.png" class="img-product-main">
            </li>
            <li>
              <img src="/img/Rectangle 77@2x.png" class="img-product-main">
            </li>
            <li>
              <img src="/img/Rectangle 75@2x.png" class="img-product-main">
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section single-product-overview">
  <div class="container">
    <h2 class="title">GUMMIES</h2>
    <div class="columns is-multiline">
      <div class="column is-4 Products-box">
        <a href="/chef-joeys/products/400mg-product-gummies/">
          <figure class="image">
            <img src="/img/product-chefjoeys-triple_tripper@2x.jpg" class="img-product-main">
            <h3 class="title">400 mg Gummy<img src="/img/fire-img.png"></h3>
          </figure>
        </a>
      </div>
      <div class="column is-4 Products-box">
        <a href="/chef-joeys/products/60mg-product-granola/">
          <figure class="image">
            <img src="/img/product-chefjoeys-triple_tripper@2x.jpg" class="img-product-main">
            <h3 class="title">60 mg Granola</h3>
          </figure>
        </a>
      </div>
      <div class="column is-4 Products-box">
        <a href="/chef-joeys/products/60mg-product-granola/">
          <figure class="image">
            <img src="/img/product-chefjoeys-triple_tripper@2x.jpg" class="img-product-main">
            <h3 class="title">60 mg Granola</h3>
          </figure>
        </a>
      </div>
    </div>
  </div>
</section>
<section class="section stay-now chef-goey-stay-now">
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-12 has-text-centered">
        <h1 class="title">STAY IN THE KNOW</h1>
        <p class="body1 mb-12">Be the first to know about new and limited products<br> and strains available in your area.</p>
        <p class="body2"><input class="input is-rounded" type="text" placeholder="">
          <a href="#"><button type="button" class="button is-primary">Subscribe</button></a>
        </p>
      </div>
    </div>
  </div>
</section>
<footer class="footer chef-goey-footer">
  <div class="container">
    <!---------------------------------------------stay know------------------------------------------------>
    <!---------------------------------------------footer chefgoey------------------------------------------------>
    <section class="section footer-delta chef-goey-footer-inner">
      <div class="container">
        <div class="columns is-multiline is-centered">
          <div class="column is-3 has-text-centered"><a href="#"><img src="/img/logo-chef_joeys-menu-logo-header.png" class="img-responsive"></a>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Products</h6>
            <ul>
              <li><a href="/chef-joeys/products/400mg-product-gummies/">400 mg Cannabis Gummy </a></li>
              <li><a href="/chef-joeys/products/50mg-product-gummies/">50 mg Cannabis Gummies</a></li>
              <li><a href="/chef-joeys/products/25mg-product-gummis/">25 mg Cannabis Gummies</a></li>
              <li><a href="/chef-joeys/products/10mg-product-gummies/">10mg Cannabis Gummies</a></li>
            </ul>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Resources</h6>
            <ul>
              <li><a href="#">Contact</a></li>
              <li><a href="#">FAQs</a></li>
            </ul>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Community</h6>
            <ul>
              <li><a href="#"><img src="/img/facebook.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="/img/instagram.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="/img/twitter.svg" class="img-responsive"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!---------------------------------------------Copyright footer chefgoey------------------------------------------------>
    <section class="footer-copyright-sec">
      <div class="container">
        <div class="columns is-multiline">
          <div class="column is-4 has-text-left">
            <p class="">© 2021, Chef Joey’s Premium Edibles. A JDMG, LLC brand.</p>
          </div>
          <div class="column is-4 has-text-centered retail-location">
            <p class="has-text-centered"><a target="_blank" href="../retail-location">Retail Locations</a></p>
          </div>
          <div class="column is-4 has-text-right">
            <ul>
              <li><a href="#">License</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </div>
</footer>