---
layout: custom-layout
brand: chefjoeys
hide_hero: true
hide_footer: true
permalink: /chef-joeys/products/
---
<nav class="navbar is-transparent chef-joeys-nav" role="navigation" aria-label="main navigation">
  <div class="container is-max-desktop">
    <div class="navbar-brand">
      <a class="navbar-item" href="chef-joeys">
        <img src="/img/logo-chef_joeys-menu-logo-header.png">
      </a>
      <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>
    <div id="navbarBasicExample" class="navbar-menu">
      <div class="navbar-start">
        <a class="navbar-item" href="/chef-joeys/products/">
          PRODUCTS
        </a>
        <a class="navbar-item" href="#">
          ABOUT THE CHEF
        </a>
        <a class="navbar-item" href="#">
          CONTACT
        </a>
      </div>
    </div>
  </div>
</nav>
<div class="col-md-12 has-text-centered chefjoey-product-top-heading">
  <h2 class="title">Browse Our Products</h2>
</div>
<section class="section chef-joeys-ourproduct-home ourproducts-page">
  <div class="container is-max-desktop">
    <h2 class="title">GUMMIES</h2>
    <div class="columns is-multiline is-centered">
      <div class="column is-6 Products-box">
        <a href="/chef-joeys/products/400mg-product-gummies/">
          <figure class="image">
            <img src="/img/product-chefjoeys-triple_tripper@2x.jpg" class="img-product-main">
            <h3 class="title">400 mg Gummy<img src="/img/fire-img.png"></h3>
          </figure>
        </a>
      </div>
      <div class="column is-6 Products-box">
        <a href="/chef-joeys/products/50mg-product-gummies/">
          <figure class="image">
            <img src="/img/product-chefjoeys-gummies-high@2x.jpg" class="img-product-main">
            <h3 class="title">50 mg Gummies</h3>
          </figure>
        </a>
      </div>
      <div class="column is-6 Products-box">
        <a href="/chef-joeys/products/25mg-product-gummis/">
          <figure class="image">
            <img src="/img/product-chefjoeys-gummies-medium@2x.jpg" class="img-product-main">
            <h3 class="title">25 mg Gummies</h3>
          </figure>
        </a>
      </div>
      <div class="column is-6 Products-box">
        <a href="/chef-joeys/products/10mg-product-gummies/">
          <figure class="image">
            <img src="/img/product-chefjoeys-gummies-mild@2x.jpg" class="img-product-main">
            <h3 class="title">10 mg Gummies</h3>
          </figure>
        </a>
      </div>
    </div>
    <h2 class="title mt-5 pt-5">GRANOLA</h2>
    <div class="columns is-multiline">
      <div class="column is-6 Products-box ff">
        <a href="/chef-joeys/products/60mg-product-granola/">
          <figure class="image">
            <img src="/img/product-chefjoeys-triple_tripper@2x.jpg" class="img-product-main">
            <h3 class="title">60 mg Granola</h3>
          </figure>
        </a>
      </div>
    </div>
  </div>
</section>
<footer class="footer chef-goey-footer">
  <div class="container">
    <!---------------------------------------------stay know------------------------------------------------>
    <!---------------------------------------------footer chefgoey------------------------------------------------>
    <section class="section footer-delta chef-goey-footer-inner">
      <div class="container">
        <div class="columns is-multiline is-centered">
          <div class="column is-3 has-text-centered"><a href="#"><img src="/img/logo-chef_joeys-menu-logo-header.png" class="img-responsive"></a>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Products</h6>
            <ul>
              <li><a href="/chef-joeys/products/400mg-product-gummies/">400 mg Cannabis Gummy </a></li>
              <li><a href="/chef-joeys/products/50mg-product-gummies/">50 mg Cannabis Gummies</a></li>
              <li><a href="/chef-joeys/products/25mg-product-gummis/">25 mg Cannabis Gummies</a></li>
              <li><a href="/chef-joeys/products/10mg-product-gummies/">10mg Cannabis Gummies</a></li>
            </ul>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Resources</h6>
            <ul>
              <li><a href="#">Contact</a></li>
              <li><a href="#">FAQs</a></li>
            </ul>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Community</h6>
            <ul>
              <li><a href="#"><img src="/img/facebook.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="/img/instagram.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="/img/twitter.svg" class="img-responsive"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!---------------------------------------------Copyright footer chefgoey------------------------------------------------>
    <section class="footer-copyright-sec">
      <div class="container">
        <div class="columns is-multiline">
          <div class="column is-4 has-text-left">
            <p class="">© 2021, Chef Joey’s Premium Edibles. A JDMG, LLC brand.</p>
          </div>
          <div class="column is-4 has-text-centered retail-location">
            <p class="has-text-centered"><a target="_blank" href="../retail-location">Retail Locations</a></p>
          </div>
          <div class="column is-4 has-text-right">
            <ul>
              <li><a href="#">License</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </div>
</footer>
<script>
$(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");

  });
});
</script>