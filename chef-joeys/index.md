---
layout: custom-layout
brand: chefjoeys
hide_hero: true
hide_footer: true
permalink: /chef-joeys/

---


<nav class="navbar is-transparent chef-joeys-nav" role="navigation" aria-label="main navigation">
  <div class="container is-max-desktop">
  <div class="navbar-brand">
    <a class="navbar-item" href="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/">
      <img src="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/assets/images/logo-chef_joeys-menu-logo-header.png">
    </a>
    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item" href="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/products/">
        PRODUCTS
      </a>
      <a class="navbar-item" href="#">
        ABOUT THE CHEF
      </a>
      <a class="navbar-item" href="#">
        CONTACT
      </a>
    </div>
  </div>
  </div>
</nav>
<section class="chefjoey-banner-home pt-5 pb-5">
  <h1 class="title has-text-centered pb-4">The finest (and strongest <img src="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/assets/images/smile.png">) cannabis fruit chews</h1>

</section>
<div class="col-md-12 has-text-centered chefjoey-banner-home-bottom-part  "> 
  <a href="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/products/">
      <button type="button" class="button is-primary">Shop Gummies</button>
  </a>
</div>
<section class="section chef-joeys-about-home">
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-7">
        <h2 class="title">About the Chef</h2>
        <p class="body1 mb-5 mt-5 pt-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
        Born and raised in New York, Chef Joey Galeano ascended through the Cannabis Industry as one of the pre-eminent Cannabis Chefs and is widely regarded as one of the industries thought-leaders. Amassing over 30 years of research and having published hundreds of recipes, he served as the National Spokesperson for Magical Butter® and most recently established the Food and Beverage Product Development road maps for several privately-held cannabis companies in the United States. </p>
        <p class="body1 mb-5">
        Since 2009, he has put on well over 500 live cooking exhibitions focused on the utilization of cannabis and cannabis derivatives. His educational tutorials, and recipe videos, which have amassed more than 250 million views on social media, are 2nd only to the personally crafted dinners that he has done for guests around the world. He’s done a kitchen takeover at Michael Jordan’s Steak House in Chicago, exclusive dinners at the X-Games in Aspen, opened the Worlds First Cannabis Food Truck in Denver Colorado, and a $100,000 Benefit Dinner Event hosted at Wayne Newton’s house in Las Vegas.</p>
        <p class="body1 mb-3">
        Above all, Chef Joey is most known for his desire to educate the consuming public…. and his Gummies.</p>
      </div>
      <div class="column is-5">
        <figure class="image"> <img src="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/assets/images/headshot-chef-joey-galleano.jpg" class="img-responsive"> </figure>
      </div>
    </div>
  </div>
</section>
<section class="section chef-joeys-ourproduct-home">
  <div class="container is-max-desktop">
      <h2 class="title">OUR PRODUCTS</h2>
    <div class="columns is-multiline is-centered">
      <div class="column is-6 Products-box">
       <a href="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/products/400mg-product-gummies/">
       <figure class="image">
        <img src="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/assets/images/product-chefjoeys-triple_tripper@2x.jpg" class="img-product-main">
        <h3 class="title">400 mg Gummy<img src="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/assets/images/fire-img.png"></h3>
      </figure> 
      </a>
      </div>
      <div class="column is-6 Products-box">
      <a href="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/products/50mg-product-gummies/">
      <figure class="image">
        <img src="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/assets/images/product-chefjoeys-gummies-high@2x.jpg" class="img-product-main">
        <h3 class="title">50 mg Gummies</h3>
      </figure>
    </a>
      </div>
      <div class="column is-6 Products-box">
       <a href="/products/25mg-product-gummis/">
       <figure class="image">
        <img src="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/assets/images/product-chefjoeys-gummies-medium@2x.jpg" class="img-product-main">
        <h3 class="title">25 mg Gummies</h3>
      </figure> 
      </a>
      </div>
      <div class="column is-6 Products-box">
      <a href="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/products/10mg-product-gummies/">
      <figure class="image">
        <img src="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/assets/images/product-chefjoeys-gummies-mild@2x.jpg" class="img-product-main">
        <h3 class="title">10 mg Gummies</h3>
      </figure>
    </a>
      </div>
      

    </div>
      
    </div>
</section>
<section class="section stay-now chef-goey-stay-now">
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-12 has-text-centered">
        <h1 class="title">STAY IN THE KNOW</h1>
        <p class="body1 mb-12">Be the first to know about new and limited products<br> and strains available in your area.</p>
       <p class="body2"><input class="input is-rounded" type="text" placeholder="">
       <a href="#"><button type="button" class="button is-primary">Subscribe</button></a>
       </p>
      </div>
    </div>
  </div>
</section>
<footer class="footer chef-goey-footer">
    <div class="container">
       <!---------------------------------------------stay know------------------------------------------------> 
<!---------------------------------------------footer chefgoey------------------------------------------------> 
<section class="section footer-delta chef-goey-footer-inner">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-3 has-text-centered"><a href="#"><img src="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/assets/images/logo-chef_joeys-menu-logo-header.png" class="img-responsive"></a>
      </div>
      <div class="column is-3">
          <h6 class="title is-6">Products</h6>
          <ul>
          <li><a href="#">400 mg Cannabis Gummy   </a></li>
            <li><a href="#">50 mg Cannabis Gummies</a></li>
            <li><a href="#">25 mg Cannabis Gummies</a></li>
            <li><a href="#">10mg Cannabis Gummies</a></li>
        </ul>
     </div>
           <div class="column is-3">
          <h6 class="title is-6">Resources</h6>
          <ul>
          <li><a href="#">Contact</a></li>
            <li><a href="#">FAQs</a></li>
        </ul>
     </div>

      <div class="column is-3">
          <h6 class="title is-6">Community</h6>
          <ul>
          <li><a href="#"><img src="../img/facebook.svg" class="img-responsive"></a></li>
            <li><a href="#"><img src="../img/instagram.svg" class="img-responsive"></a></li>
            <li><a href="#"><img src="../img/twitter.svg" class="img-responsive"></a></li>
        </ul>
     </div>
  </div>
</div>
</section>
<!---------------------------------------------Copyright footer chefgoey------------------------------------------------> 
<section class="footer-copyright-sec">
  <div class="container">
    <div class="columns is-multiline">
      <div class="column is-4 has-text-left">
        <p class="">© 2021, Chef Joey’s Premium Edibles. A JDMG, LLC brand.</p>
      </div>
     <div class="column is-4 has-text-centered retail-location">
        <p class="has-text-centered"><a target="_blank" href="../retail-location">Retail Locations</a></p>
      </div>
      <div class="column is-4 has-text-right">
        <ul>
          <li><a href="#">License</a></li>
          <li><a href="#">Terms</a></li>
          <li><a href="#">Privacy</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
    

    </div>
</footer>
<script>
  $(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      $(".navbar-burger").toggleClass("is-active");
      $(".navbar-menu").toggleClass("is-active");

  });
});
</script>
