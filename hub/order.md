---
layout: custom-layout
brand: hub
hide_hero: true
permalink: /hub/order/
---
<section class="section order-request">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h1 class="title">Order Request</h1>
        {% include header-logos.html %}
      </div>
    </div>
  </div>
</section>
<!---------------------------------------------Personal wholesale------------------------------------------------>
<section class="section personal-wholesale">
  <div class="container is-max-desktop">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h2 class="title">Personal or Wholesale?</h2>
        <p class="body1 mb-3">Thank you for your interest in our products. Would you like to buy for personal consumption or are you buying<br> wholesale on behalf of a licensed Oklahoma dispensary/business?</p>
        <p class="has-text-centered">
          <a href="{{ site.url-hub }}{{ site.devbaseurl-hub }}/order/personal/"><button type="button" class="button is-info is-rounded">Personal Consumption</button></a>
          <span><a href="{{ site.url-hub }}{{ site.devbaseurl-hub }}/order/wholesale/"><button type="button" class="button is-info is-rounded">Business Wholesale</button></a></span>
        </p>
      </div>
    </div>
  </div>
</section>
<!---------------------------------------------stay know------------------------------------------------>