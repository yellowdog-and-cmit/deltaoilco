---
layout: custom-layout
brand: hub
hide_hero: true
permalink: /hub/order/personal/
---
<section class="section order-request">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h1 class="title">Order Request</h1>
        {% include header-logos.html %}
      </div>
    </div>
  </div>
</section>
<!---------------------------------------------consumer steps 2------------------------------------------------>
<section class="section personal-wholesale">
  <div class="container is-max-desktop">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h2 class="title">Step 2 of 2</h2>
        <p class="body1 mb-3">Unfortunately, we are legally not allowed to sell our products directly to consumers.<span> Check our list of retail locations</span> for one near you. Don’t see one in your area? That’s okay. Our products are being offered by more and more dispensaries everyday. Please let us know where you buy your product, and if possible, in which products you are most interested. We’ll do our best to get them stocked near you as soon as possible!</p>
        <i class="body1 mb-5 italic-text">By completing this form, you are NOT subscribing to anything unless you explicitly opt-in for updates/news.By default, this information will only be used to improve our product offering and availability. Thank you for your feedback.</i>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">In which zip code do you buy?</label>
            <div class="control">
              <input class="input" type="number" placeholder="">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">Name of your preferred dispensary?</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
        </div>
        <h6 class="title is-6">In which products are you interested? (check all that apply)</h6>
        <div class="products-quentity-main">
          <div class="products-quentity">
            <h3 class="title mt-0">EDIBLES</h3>
            <div class="form-group">
              <input type="checkbox" id="Granola">
              <label for="Granola">Granola (Chef Joey’s)</label>
            </div>
            <div class="form-group">
              <input type="checkbox" id="Gummies">
              <label for="Gummies">Gummies (Chef Joey’s)</label>
            </div>
            <br>
            <h3 class="title mt-0">DABABLES</h3>
            <div class="form-group">
              <input type="checkbox" id="LiveResin">
              <label for="LiveResin">Live Resin Batter (Nature’s Lab)</label><br>
            </div>
            <div class="form-group">
              <input type="checkbox" id="Crumble">
              <label for="Crumble">Crumble (Delta Oil Co.)</label><br>
            </div>
            <div class="form-group">
              <input type="checkbox" id="Live">
              <label for="Live">Live Batter (Nature’s Lab)</label><br>
            </div>
            <div class="form-group">
              <input type="checkbox" id="LiveSugar">
              <label for="LiveSugar">Live Sugar (Nature’s Lab)</label><br>
            </div>
            <div class="form-group">
              <input type="checkbox" id="Cured">
              <label for="Cured">Cured Batter (Delta Oil Co.)</label><br>
            </div>
            <h3 class="title">CARTRIDGES</h3>
            <div class="form-group">
              <input type="checkbox" id="C-Cell">
              <label for="C-Cell">C-Cell Cartridges</label><br>
            </div>
            <div class="form-group">
              <input type="checkbox" id="510Thread">
              <label for="510Thread">510 Thread Carts</label><br>
            </div>
            <h3 class="title">FECO/TINCTURES</h3>
            <div class="form-group">
              <input type="checkbox" id="RSO">
              <label for="RSO">RSO Syringe</label><br>
            </div>
            <div class="form-group">
              <input type="checkbox" id="TinctureTHC">
              <label for="TinctureTHC">Tincture - THC Oral Solution</label><br>
            </div>
            <div class="form-group">
              <input type="checkbox" id="TinctureCBD">
              <label for="TinctureCBD">Tincture - CBD Oral Solution</label><br>
            </div>
            <div class="form-group">
              <input type="checkbox" id="THCCBD">
              <label for="THCCBD">Tincture - THC:CBD Mix Oral Solution</label><br>
            </div>
            <h3 class="title">TOPICALS</h3>
            <div class="form-group">
              <input type="checkbox" id="Muscle">
              <label for="Muscle">Muscle & Joint Pain Relief Rub (Body Guac)</label><br>
            </div>
          </div>
          <br><br>
          <h6 class="title is-6 mb-3">Optional opt-ins:</h6>
          <div class="form-group">
            <input type="checkbox" id="Notifyme">
            <label for="Notifyme">Notify me when products are available in my zip code.</label><br>
          </div>
          <div class="form-group">
            <input type="checkbox" id="Joinour">
            <label for="Joinour">Join our mailing list for updates about new products. Don’t worry. We hate SPAM too.</label><br>
          </div>
          <p class="body2 mt-3"><input class="input is-medium" type="text" placeholder="your@email.com">
            <a href="#"><button type="button" class="button is-info">Submit</button></a></p>
        </div>
      </div>
    </div>
  </div>
</section>