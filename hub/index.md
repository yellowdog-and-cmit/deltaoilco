---
layout: default
brand: hub
hide_hero: true
hide_footer: true
permalink: /hub/
---
<section class="home-logo-title">
  <div class="container">
    <div class="row">
      <div class="col-md-12 has-text-centered hero-body">
        <div class="logo-title"> <img src="assets/images/logo-delta_oil_co-primary-RGB@2x.png" class="img-responsive" />
          <h6 class="title is-6">JDMG, LLC</h6>
          <p class="mt-5 mb-5">Welcome to Delta Oil Co. and the JDMG, LLC family of brands. Through our vertically-integrated supply chain<br>
            and family of premium brands we are committed to being the leading multi-phase medical marijuana<br>
            company within the state of Oklahoma.</p>
          <a href="{{ site.url-hub }}{{ site.devbaseurl-hub }}/order/">
            <button type="button" class="button is-info is-rounded">Buy Our Products</button>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home-family-of-brands-section">
  <div class="container is-max-desktop">
    <h2 class="has-text-centered title is-4 has-text-weight-bold mb-5">Explore our family of brands</h2>
    <div class="columns is-multiline is-centered">
      <div class="column is-3 has-text-centered"> <a href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/"><img src="assets/images/logo-delta_oil_co-primary.svg" class="img-responsive" /></a> </div>
      <div class="column is-3 has-text-centered"> <a href="{{ site.url-chefjoeys }}{{ site.devbaseurl-chefjoeys }}/"><img src="assets/images/logo-chef_joeys-primary-RGB.png" class="img-responsive" /></a> </div>
      <div class="column is-3 has-text-centered"> <a href="{{ site.url-bodyguac }}{{ site.devbaseurl-bodyguac }}/"><img src="assets/images/logo-body_guac-primary.svg" class="img-responsive" /></a> </div>
      <div class="column is-3 has-text-centered"> <a href="{{ site.url-natureslab }}{{ site.devbaseurl-natureslab }}/"><img src="assets/images/logo-natures_lab-primary.svg" class="img-responsive" /></a> </div>
    </div>
  </div>
</section>
<section class="footer-copyright-sec">
  <div class="container">
    <div class="columns is-multiline">
      <div class="column is-4 has-text-left">
        <p class="">© 2021, JDMG, LLC. All rights reserved.</p>
      </div>
      <div class="column is-4 has-text-centered retail-location">
        <p class="has-text-centered"><a target="_blank" href="/retail-locations/">Retail Locations</a></p>
      </div>
      <div class="column is-4 has-text-right">
        <ul>
          <li><a href="#">Privacy</a></li>
          <li><a href="#">Terms</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>