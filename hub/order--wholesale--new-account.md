---
layout: custom-layout
brand: hub
hide_hero: true
permalink: /hub/order/wholesale/new-account/
---
<section class="section order-request">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h1 class="title">Order Request</h1>
        {% include header-logos.html %}
      </div>
    </div>
  </div>
</section>
<!------------------------------------------------New Account------------------------------------------------------>
<section class="section personal-wholesale">
  <div class="container is-max-desktop">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h2 class="title">New Account</h2>
        <p class="body1 mb-3">We are stoked that you would like to sell our products at your dispensary! Please tell us about your operation. One of our reps will contact you to setup an account and work out the details.</p>
        <h3 class="title">BUSINESS INFORMATION</h3>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">Business Name</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">Business Type</label>
            <div class="select">
              <select>
                <option>Select a business type</option>
                <option>With options</option>
              </select>
            </div>
          </div>
        </div>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">Oklahoma License Number</label>
            <div class="control">
              <input class="input" type="text" placeholder="Eg. DAAA-9A9A-9A9A">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">License Type</label>
            <div class="select">
              <select>
                <option>Select a license type</option>
                <option>With options</option>
              </select>
            </div>
          </div>
        </div>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">Address Line 1</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">Address Line 2</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
        </div>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">City</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">Zip Code</label>
            <div class="control">
              <input class="input" type="number" placeholder="">
            </div>
          </div>
        </div>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">Your website URL</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">Weedmaps.com profile URL</label>
            <div class="control">
              <input class="input" type="text" placeholder="Eg. https://weedmaps.com/dispensaries/you">
            </div>
          </div>
        </div>
        <h3 class="title">CONTACT PERSON</h3>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">First Name</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">Last Name</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
        </div>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">Phone Number</label>
            <div class="control">
              <input class="input" type="number" placeholder="">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">Email</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
        </div>
        <div class="columns is-desktop">
          <div class="field column is-6">
            <label class="label">Business Role</label>
            <div class="select">
              <select>
                <option>Select a business role</option>
                <option>With options</option>
              </select>
            </div>
          </div>
        </div>
        <h3 class="sub-title">ADDITIONAL INFO</h3>
        <p class="body1 mb-3 mt-2">Anything else we should know?</p>
        <div class="control">
          <textarea class="textarea" placeholder=""></textarea>
        </div>
        <p class="body1 mb-3 use-mobile">By submitting this form, I certify that I am an authorized representative of this business, and the information I have provided here is accurate to the best of my knowledge.</p>
        <a href="#">
          <button type="button" class="button is-info is-rounded">Submit</button>
        </a>
      </div>
    </div>
  </div>
</section>