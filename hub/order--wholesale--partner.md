---
layout: custom-layout
brand: hub
hide_hero: true
permalink: /hub/order/wholesale/partner/
---
<section class="section order-request">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h1 class="title">Order Request</h1>
        {% include header-logos.html %}
      </div>
    </div>
  </div>
</section>
<!------------------------------------------------Customer------------------------------------------------------>
<section class="section personal-wholesale">
  <div class="container is-max-desktop">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h2 class="title">Step 3 of 3</h2>
        <p class="body1 mb-3">Howdy, partner. Please fill out this order form, and one of our reps will contact you ASAP to confirm your order and pricing and to arrange delivery and payment.</p>
        <h3 class="title">DISPENSARY INFORMATION</h3>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">Dispensary Name</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">OMMA License Number</label>
            <div class="control">
              <input class="input" type="text" placeholder="Eg. DAAA-9A9A-9A9A">
            </div>
          </div>
        </div>
        <h3 class="title">CONTACT PERSON</h3>
        <div class="columns is-desktop is-centered">
          <div class="field column is-6">
            <label class="label">Contact Name</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
          <div class="field column is-6">
            <label class="label">Contact Phone</label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
        </div>
        <div class="columns is-desktop">
          <div class="field column is-12">
            <label class="label">Contact Email Address
              <br /><span style="font-weight: normal;">We will contact you at this address to confirm your order.</span></label>
            <div class="control">
              <input class="input" type="text" placeholder="">
            </div>
          </div>
        </div>
        <div class="order-form-table">
          <div class="order-form-table-main-heading">
            <h2 class="title mb-5">ORDER FORM</h2>
          </div>
          <div class="order-form-table-outer">
            <h3 class="title mt-0">EDIBLES</h3>
            <div class="order-form-table-sub-title">
              <h4 class="title">GRANOLA</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>FLAVOR</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>Berry Nuts</td>
                    <td>60mg</td>
                    <td>Chef Joey’s Granola</td>
                    <td>$12.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>CoCo Kush</td>
                    <td>60mg</td>
                    <td>Chef Joey’s Granola</td>
                    <td>$12.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Glaze of Fire</td>
                    <td>60mg</td>
                    <td>Chef Joey’s Granola</td>
                    <td>$12.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="order-form-table-sub-title">
              <h4 class="title">10mg GUMMIES</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>FLAVOR</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>Blueberry Punch</td>
                    <td>100mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$18.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Key Lime Pie</td>
                    <td>100mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$18.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Mango</td>
                    <td>100mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$18.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Pineapple</td>
                    <td>100mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$18.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Strawberry</td>
                    <td>100mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$18.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="order-form-table-sub-title">
              <h4 class="title">25mg GUMMIES</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>FLAVOR</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>Blueberry Punch</td>
                    <td>250mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$25.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Key Lime Pie</td>
                    <td>250mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$25.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Mango</td>
                    <td>250mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$25.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Pineapple</td>
                    <td>250mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$25.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Strawberry</td>
                    <td>250mg</td>
                    <td>Chef Joey’s Gummies, 10 piece bag</td>
                    <td>$25.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="order-form-table-sub-title">
              <h4 class="title">SPECIALTY GUMMIES</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>FLAVOR</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>Triple Tripper</td>
                    <td>400mg</td>
                    <td>Chef Joey’s Gummy, single pack bag</td>
                    <td>$32.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>50mg Assorted</td>
                    <td>1,000mg</td>
                    <td>Chef Joey’s Gummies, 20 piece bag</td>
                    <td>$55.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <h3 class="title mt-5">DABABLES</h3>
            <div class="order-form-table-sub-title">
              <h4 class="title">CONCENTRATES</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>SKU</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>Batter</td>
                    <td>1 gram</td>
                    <td>Natures Lab Live Resin Batter</td>
                    <td>$25.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Golden Berry</td>
                    <td>1 gram</td>
                    <td>Delta Oil Co. Crumble</td>
                    <td>$25.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Dosido Batter</td>
                    <td>1 gram</td>
                    <td>Natures Lab Live Batter</td>
                    <td>$25.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Dosido LIVE Sugar</td>
                    <td>1 gram</td>
                    <td>Natures Lab Live Sugar</td>
                    <td>$25.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Gelato Cake</td>
                    <td>1 gram</td>
                    <td>Delta Oil Co. Cured Batter</td>
                    <td>$25.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <h3 class="title mt-5">CARTRIDGES</h3>
            <div class="order-form-table-sub-title">
              <h4 class="title">C-CELL CARTS</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>SKU</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>Strain Specific Sauce</td>
                    <td>0.5 gram</td>
                    <td>C-Cell Cartridges</td>
                    <td>$45.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Blood Orange</td>
                    <td>1 gram</td>
                    <td>C-Cell Cartridges</td>
                    <td>$35.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Sour Apple</td>
                    <td>1 gram</td>
                    <td>C-Cell Cartridges</td>
                    <td>$35.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Strawberry Banana</td>
                    <td>1 gram</td>
                    <td>C-Cell Cartridges</td>
                    <td>$35.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Watermelon</td>
                    <td>1 gram</td>
                    <td>C-Cell Cartridges</td>
                    <td>$35.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Grape Ape</td>
                    <td>1 gram</td>
                    <td>C-Cell Cartridges</td>
                    <td>$35.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="order-form-table-sub-title">
              <h4 class="title">510 THREAD CARTS</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>FLAVOR</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>Bruce Banner Strain Specific</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Cookies Strain Specific</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Durban Poison Strain Specific</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Jack Herer Strain Specific</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Lemon Cake Strain Specific</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Lemon Garlic OG Strain Specific</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Sour Tangie Strain Specific</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Strawnana Strain Specific</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>The Mac Strain Specific</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Black Cherry</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Blueberry Banana</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Bubblegum</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Cherry Limeade</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Fruit Punch</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Grape</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Peach</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Pina Colada</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Strawberry Shortcake</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Watermelon Gum</td>
                    <td>1 gram</td>
                    <td>510 Thread</td>
                    <td>$30.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <h3 class="title mt-5">FECO/TINCTURES</h3>
            <div class="order-form-table-sub-title">
              <h4 class="title">FECO/RSO</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>FLAVOR</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>Grand Daddy Purple (Indica)</td>
                    <td>1 gram</td>
                    <td>RSO Syringe</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Grand Daddy Purple (Indica)</td>
                    <td>3 gram</td>
                    <td>RSO Syringe</td>
                    <td>$75.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Pineapple Express (Sativa)</td>
                    <td>1 gram</td>
                    <td>RSO Syringe</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Pineapple Express (Sativa)</td>
                    <td>3 gram</td>
                    <td>RSO Syringe</td>
                    <td>$75.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>OG Kush (Indica)</td>
                    <td>1 gram</td>
                    <td>RSO Syringe</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>OG Kush (Indica)</td>
                    <td>3 gram</td>
                    <td>RSO Syringe</td>
                    <td>$75.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Blue Dream (Sativa)</td>
                    <td>1 gram</td>
                    <td>RSO Syringe</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Blue Dream (Sativa)</td>
                    <td>3 gram</td>
                    <td>RSO Syringe</td>
                    <td>$75.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Gorilla Glue #4 (Hybrid)</td>
                    <td>1 gram</td>
                    <td>RSO Syringe</td>
                    <td>$30.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Gorilla Glue #4 (Hybrid)</td>
                    <td>3 gram</td>
                    <td>RSO Syringe</td>
                    <td>$75.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="order-form-table-sub-title">
              <h4 class="title">TINCTURES</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>SKU</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>1:1</td>
                    <td>30ml / 1000mg</td>
                    <td>THC:CBD Oral Solution</td>
                    <td>$45.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>1:0</td>
                    <td>30ml / 1000mg</td>
                    <td>THC Oral Solution</td>
                    <td>$40.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>0:1</td>
                    <td>30ml / 1000mg</td>
                    <td>CBD Oral Solution</td>
                    <td>$35.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>5:1</td>
                    <td>30ml / 1200mg</td>
                    <td>THC:CBD Oral Solution</td>
                    <td>$65.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>1:5</td>
                    <td>30ml / 1200mg</td>
                    <td>THC:CBD Oral Solution</td>
                    <td>$55.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>10:1</td>
                    <td>30ml / 1100mg</td>
                    <td>THC:CBD Oral Solution</td>
                    <td>$75.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>1:10</td>
                    <td>30ml / 1100mg</td>
                    <td>THC:CBD Oral Solution</td>
                    <td>$60.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <h3 class="title mt-5">TOPICALS</h3>
            <div class="order-form-table-sub-title">
              <h4 class="title">BODY GUAC</h4>
              <table class="table">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>FLAVOR</th>
                    <th>PKG SIZE</th>
                    <th>DESCRIPTION</th>
                    <th>MSRP</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span>0</span></td>
                    <td>Body Guac - Pain Reliever</td>
                    <td>.3oz(SMPL)</td>
                    <td>400mg Cannabinoids</td>
                    <td>SAMPLE</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Body Guac - Pain Reliever</td>
                    <td>1 oz</td>
                    <td>1,750mg THC | 1,750mg CBD</td>
                    <td>$125.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Body Guac - Pain Reliever</td>
                    <td>1.7 oz</td>
                    <td>2,975mg THC | 2,975mg CBD</td>
                    <td>$179.00</td>
                  </tr>
                  <tr>
                    <td><span>0</span></td>
                    <td>Body Guac - Pain Reliever</td>
                    <td>2.2 oz</td>
                    <td>3,850mg THC | 3,850mg CBD</td>
                    <td>$199.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="textrea-section">
            <h3 class="title textarea-heading">ADDITIONAL INFO</h3>
            <p class="body1 mb-3">Please note any special instructions or any deviations from your standard delivery info.</p>
            <div class="field">
              <div class="control">
                <textarea class="textarea" placeholder="Textarea"></textarea>
              </div>
              <a href="#">
                <button type="button" class="button is-info is-rounded">Submit</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!---------------------------------------------stay in the know------------------------------------------------>