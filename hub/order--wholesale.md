---
layout: custom-layout
brand: hub
hide_hero: true
permalink: /hub/order/wholesale/
---
<section class="section order-request">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h1 class="title">Order Request</h1>
        {% include header-logos.html %}
      </div>
    </div>
  </div>
</section>
<!---------------------------------------------consumer steps 2------------------------------------------------>
<section class="section personal-wholesale">
  <div class="container is-max-desktop">
    <div class="columns is-multiline is-centered">
      <div class="column is-12">
        <h2 class="title">Step 2 of 3</h2>
        <p class="body1 mb-3">Great. Let’s do this. Are you a current dispensary partner or would you like to setup a new account with us and start selling our brands?</p>
        <p class="has-text-centered">
          <a href="{{ site.url-hub }}{{ site.devbaseurl-hub }}/order/wholesale/partner/"><button type="button" class="button is-info is-rounded">Current Partner</button></a>
          <span><a href="{{ site.url-hub }}{{ site.devbaseurl-hub }}/order/wholesale/new-account/"><button type="button" class="button is-info is-rounded">New Account</button></a></span>
        </p>
      </div>
    </div>
  </div>
</section>