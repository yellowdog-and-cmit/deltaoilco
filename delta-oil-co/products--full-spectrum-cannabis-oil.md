---
layout: custom-layout
brand: deltaoilco
hide_hero: true
hide_footer: true
permalink: /delta-oil-co/products/full-spectrum-cannabis-oil/
---
<section class="about-navbar">
  <div class="about-navbar-logo mt-5">
    <img src="../../../img/logo-delta_oil_co-horizontal.svg" width="430" height="70">
  </div>
  <nav class="navbar is-transparent delta-oil-co-nav" role="navigation" aria-label="main navigation">
    <div class="container is-max-desktop">
      <div class="navbar-brand">
        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
      <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">
          <a class="navbar-item" href="/main/">HOME</a>
          <a class="navbar-item" href="/main/products/">PRODUCTS</a>
          <a class="navbar-item" href="#">ABOUT</a><a class="navbar-item" href="#">CONTACT</a>
        </div>
      </div>
    </div>
  </nav>
</section>
<section class="section product-page-product-content-section delta-product-single-page-warp">
  <div class="container">
    <div class="columns is-multiline is-centered">
      <div class="column is-5 Products-left-images">
        <a href="#">
          <figure class="image">
            <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
          </figure>
        </a>
        <ul class="products-left-light-image">
          <li><a href="#">
              <figure class="image">
                <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
          <li><a href="#">
              <figure class="image">
                <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
              </figure>
            </a></li>
        </ul>
      </div>
      <div class="column is-7 Products-right-content">
        <h3 class="title">Full Spectrum Cannabis Oil</h3>
        <p class="body1 mb-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
        <p class="body1 mb-5"> Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. 10 PIECES PER BAG, 25 mg THC PER PIECE Net Weight 26 g (0.917 oz), 2.6 g Per Piece</p>
        <p class="body1 mb-12">10 PIECES PER BAG, 25 mg THC PER PIECE Net Weight 26 g (0.917 oz), 2.6 g Per Piece</p>
        <div class="Products-right-content-select-product">
          <h4 class="title is-4">Flavors</h4>
          <div class="select">
            <select>
              <option>Blueberry Punch</option>
              <option>With options</option>
            </select>
          </div>
          <a href="http://127.0.0.1:4000/order/">
            <button type="button" class="button is-primary">Request to Order</button>
          </a>
        </div>
        <div class="product-short-detail-logo-inner">
          <ul>
            <li>
              <img src="/img/Rectangle 73@2x.png" class="img-product-main">
            </li>
            <li>
              <img src="/img/Rectangle 70@2x.png" class="img-product-main">
            </li>
            <li>
              <img src="/img/Rectangle 77@2x.png" class="img-product-main">
            </li>
            <li>
              <img src="/img/Rectangle 75@2x.png" class="img-product-main">
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section single-product-overview delta-single-product-overview-wrap">
  <div class="container">
    <h2 class="title">GUMMIES</h2>
    <div class="columns is-multiline">
      <div class="column is-4 Products-box">
        <a href="/main/products/flavored-vape-cartridges/">
          <figure class="image">
            <img src="/img/product-delta-cartridges@2x.jpg" class="img-product-main">
            <h3 class="title">Flavored Vape Cartridges</h3>
          </figure>
        </a>
      </div>
      <div class="column is-4 Products-box">
        <a href="/main/products/concentrate/">
          <figure class="image">
            <img src="/img/product-delta-concentrate@2x.jpg" class="img-product-main">
            <h3 class="title">Concentrate</h3>
          </figure>
        </a>
      </div>
      <div class="column is-4 Products-box">
        <a href="/main/products/full-spectrum-cannabis-oil/">
          <figure class="image">
            <img src="/img/product-delta-oil@2x.jpg" class="img-product-main">
            <h3 class="title">Flavored Vape Cartridges</h3>
          </figure>
        </a>
      </div>
    </div>
  </div>
</section>
<section class="section stay-now delta-oil-single-product-sec">
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-12 has-text-centered">
        <h1 class="title">Stay in the know</h1>
        <p class="body1 mb-12">Be the first to know about new and limited products<br> and strains available in your area.</p>
        <p class="body2"><input class="input is-rounded" type="text" placeholder="">
          <a href="#"><button type="button" class="button is-primary">Subscribe</button></a>
        </p>
      </div>
    </div>
  </div>
</section>
<footer class="footer">
  <div class="container">
    <!---------------------------------------------stay know------------------------------------------------>
    <!---------------------------------------------footer delta------------------------------------------------>
    <section class="section footer-delta">
      <div class="container">
        <div class="columns is-multiline is-centered">
          <div class="column is-3 has-text-centered"><a href="#"><img src="../../../img/logo-delta_oil_co-primary-RGB.png" class="img-responsive"></a>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Our Brands</h6>
            <ul>
              <li><a href="#">Delta Oil Co.</a></li>
              <li><a href="#">Chef Joey’s Premium Edibles</a></li>
              <li><a href="#">Body Guac</a></li>
              <li><a href="#">Nature’s Lab Oklahoma</a></li>
            </ul>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Resources</h6>
            <ul>
              <li><a href="#">Where to Buy</a></li>
              <li><a href="#">Order Products</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">FAQs</a></li>
            </ul>
          </div>
          <div class="column is-3">
            <h6 class="title is-6">Community</h6>
            <ul>
              <li><a href="#"><img src="../../../img/facebook.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="../../../img/instagram.svg" class="img-responsive"></a></li>
              <li><a href="#"><img src="../../../img/twitter.svg" class="img-responsive"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!---------------------------------------------Copyright footer delta------------------------------------------------>
    <section class="footer-copyright-sec">
      <div class="container">
        <div class="columns is-multiline">
          <div class="column is-4 has-text-left">
            <p class="">© 2021, JDMG, LLC. All rights reserved.</p>
          </div>
          <div class="column is-4 has-text-centered retail-location">
            <p class="has-text-centered"><a target="_blank" href="../retail-location">Retail Locations</a></p>
          </div>
          <div class="column is-4 has-text-right">
            <ul>
              <li><a href="#">Privacy</a></li>
              <li><a href="#">Terms</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </div>
</footer>
<script>
$(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");

  });
});
</script>