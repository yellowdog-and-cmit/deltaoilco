---
layout: custom-layout
brand: deltaoilco
hide_hero: true
permalink: /delta-oil-co/
---


<section class="about-banner-section">
<section class="about-navbar">
<div class="about-navbar-logo">
<img src="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/assets/images/logo-delta_oil_co-horizontal.svg" width="430" height="70">
</div>

<nav class="navbar is-transparent delta-oil-co-nav" role="navigation" aria-label="main navigation">
  <div class="container is-max-desktop">
  <div class="navbar-brand">
    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item" href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/">HOME</a>
      <a class="navbar-item" href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/products/">PRODUCTS</a>
      <a class="navbar-item" href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/#about">ABOUT</a>
      <a class="navbar-item" href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/contact/">CONTACT</a>
    </div>

    
  </div>
  </div>
</nav>
</section>

  <div class="container is-max-desktop">
    <div class="columns">
      <div class="column is-7">
        <div class="content is-normal mt-5 pt-5">
          <p>FEATURED</p>
          <h1 class="title mb-5 pb-3 mt-4">Full spectrum cannabis<br>
            in all the right mixes</h1>
          <a href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/products/full-spectrum-cannabis-oil/">
          <button type="button" class="button is-info is-rounded">View Product</button>
          </a> </div>
      </div>
      <div class="column is-4">
        <figure class="image"> <img src="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/assets/images/mockup-delta-tincture_oil-bottle-box-transparent.png" class="img-responsive"> </figure>
      </div>
    </div>
  </div>
</section>

<section class="featured-product-categories pt-5 pb-5">
  <h1 class="title has-text-centered pb-4">Featured Product Categories</h1>
  <div class="container">
    <div class="columns">
      <div class="column is-4">
        <div class="card">
          <div class="card-content">
            <div class="content">
              <a href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/products/full-spectrum-cannabis-oil/"><figure class="image"> <img src="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/assets/images/product-delta-oil@2x.jpg" class="img-responsive"> </figure></a>
            </div>
          </div>
        </div>
        <h3 class="title pt-3 pd-5 mb-5"><a href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/products/full-spectrum-cannabis-oil/">Full Spectrum Cannabis Oil</a></h3>
      </div>
      <div class="column is-4">
        <div class="card">
          <div class="card-content">
            <div class="content">
              <a href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/products/flavored-vape-cartridges/"><figure class="image"> <img src="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/assets/images/product-delta-cartridges@2x.jpg" class="img-responsive"> </figure></a>
            </div>
          </div>
        </div>
        <h3 class="title pt-3 pd-5 mb-5"><a href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/products/flavored-vape-cartridges/">Flavored Vape Cartridges</a></h3>
      </div>
      <div class="column is-4">
        <div class="card">
          <div class="card-content">
            <div class="content">
              <a href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/products/concentrate/"><figure class="image"> <img src="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/assets/images/product-delta-concentrate@2x.jpg" class="img-responsive"> </figure></a>
            </div>
          </div>
        </div>
        <h3 class="title pt-3 pd-5 mb-5"><a href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/products/concentrate/">Concentrate</a></h3>
      </div>
    </div>
    <div class="col-md-12 has-text-centered"> <a href="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/products/">
      <button type="button" class="button is-primary">View All Products</button>
      </a> </div>
  </div>
</section>

<section class="section about-delta">
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-6">
        <h2 class="title" style="position: relative;">About Delta Oil Co.
        <a id="about" style="position:absolute; top:-250px"></a>
        </h2>
        <p class="body1 mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
        <p class="body1 mb-3">Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
        <p class="body1">Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>
      </div>
      <div class="column is-6">
        <figure class="image"> <img src="{{ site.url-deltaoilco }}{{ site.devbaseurl-deltaoilco }}/assets/images/photo-about-delta.png" class="img-responsive"> </figure>
      </div>
    </div>
  </div>
</section>


<section class="Accordion">
   <h2 class="title">Frequently Asked Questions</h2>
   <div class="container is-max-desktop">
	  <ul class="Accordion__tabs">
		<li class="Accordion__tab" onclick="toggleAccordion(this)">
		  <div class="Accordion__tab__headline">
			<h4>Lorem ipsum dolor sit amet</h4><span class="icon"></span>
		  </div>
		  <div class="Accordion__tab__content">
			<div class="wrapper">
			  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor 
			  </p>
			</div>
		  </div>
		</li>
		<li class="Accordion__tab" onclick="toggleAccordion(this)">
		  <div class="Accordion__tab__headline">
			<h4>dolor sit amet</h4><span class="icon"></span>
		  </div>
		  <div class="Accordion__tab__content">
			<div class="wrapper">
			  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, 
	consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
			  </p>
			  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
			  </p>
			</div>
		  </div>
		</li>
		<li class="Accordion__tab" onclick="toggleAccordion(this)">
		  <div class="Accordion__tab__headline">
			<h4>ipsum dolor sit amet</h4><span class="icon"></span>
		  </div>
		  <div class="Accordion__tab__content">
			<div class="wrapper">
			  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
			  </p>
			</div>
		  </div>
		</li>
		<li class="Accordion__tab" onclick="toggleAccordion(this)">
		  <div class="Accordion__tab__headline">
			<h4>Lorem ipsum</h4><span class="icon"></span>
		  </div>
		  <div class="Accordion__tab__content">
			<div class="wrapper">
			  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. 
			  </p>
			</div>
		  </div>
		</li>
	  </ul>
	  </div>
</section>
<script>
  $(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      $(".navbar-burger").toggleClass("is-active");
      $(".navbar-menu").toggleClass("is-active");

  });
});
</script>