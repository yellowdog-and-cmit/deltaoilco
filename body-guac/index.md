---
layout: custom-layout
brand: bodyguac
hide_hero: true
hide_footer: true
permalink: /body-guac/
---
<nav class="navbar is-transparent body-guac-nav" role="navigation" aria-label="main navigation">
  <div class="container is-max-desktop">
    <div class="navbar-brand">
      <a class="navbar-item" href="body-guas">
        <img src="{{ site.url-bodyguac }}{{ site.devbaseurl-bodyguac }}/assets/images/logo-body_guac-primary.svg">
      </a>
      <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>
  </div>
</nav>
<section class="body-guac-content-main">
  <div class="container is-max-desktop">
    <div class="row">
      <div class="col-md-12 has-text-centered hero-body">
        <h5 class="title mt-5 mb-5">
          PROFESSIONAL STRENGTH • FULL SPECTRUM</h5>
        <h2 class="title">CANNABIS TOPICAL RUB</h2>
        <figure class="image">
          <img src="{{ site.url-bodyguac }}{{ site.devbaseurl-bodyguac }}/assets/images/mockup-box-delta-body_guac-angle-top_front_left.png" class="image-responsive">
        </figure>
        <p class="mt-5 mb-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
      </div>
    </div>
  </div>
</section>
<section class="section body-guac-comingsoon-sec">
  <div class="container is-max-desktop">
    <div class="columns is-desktop is-centered">
      <div class="column is-12 has-text-centered">
        <h1 class="title">COMING SOON</h1>
        <p class="body1 mb-12">Get on the list and be notified of the Body Guac product launch.</p>
        <p class="body2"><input class="input" type="text" placeholder="">
          <a href="#"><button type="button" class="button is-primary">Subscribe</button></a>
        </p>
      </div>
    </div>
  </div>
</section>
<footer class="footer chef-goey-footer body-guas-footer-main">
  <div class="container">
    <!---------------------------------------------stay know------------------------------------------------>
    <!---------------------------------------------footer chefgoey------------------------------------------------>
    <!---------------------------------------------Copyright footer chefgoey------------------------------------------------>
    <section class="footer-copyright-sec">
      <div class="container">
        <div class="columns is-multiline">
          <div class="column is-4 has-text-left">
            <p class="">© 2021, Body Guac. A JDMG, LLC brand.</p>
          </div>
          <div class="column is-4 has-text-centered retail-location">
            <p class="has-text-centered"><a target="_blank" href="../retail-location">Retail Locations</a></p>
          </div>
          <div class="column is-4 has-text-right">
            <ul>
              <li><a href="#">License</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </div>
</footer>
<script>
$(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");

  });
});
</script>